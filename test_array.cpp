#include <cstdio>
#include <iostream>
#include <fstream>
#include "Array.h"

int main()
{
    try
    {
        DynamicArray<int>  array(1, 5);
        array.resize( 10 );

        array.push( 6 );

        std::cout << array[0];
        std::cout << array[1];

        array[0] = 4;
        std::cout << array[20];
    }
    catch( ChainedException* e )
    {
        std::ofstream   out( "Result.txt" );
        e->dump( stdout );
        drop_chain(e);
    }
    return 0;
}
