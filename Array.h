#ifndef TRACK_CPP_2_SEM_ARRAY_H
#define TRACK_CPP_2_SEM_ARRAY_H

#include <cstdint>
#include <cassert>
#include "ChainedException.h"

#define pure = 0

template <typename Data_T>
class Array
{
protected:
    size_t      _size   = 0;

public:
    
    virtual Data_T&         operator[]( int index )       pure;
    virtual const Data_T&   operator[]( int index ) const pure;
    
    virtual bool            valid()                 const pure;
    virtual size_t          size()                  const pure;
};

template <>
class Array<bool>
{
protected:
    size_t      _size   = 0;

    class Proxy
    {
    private:
        int         _shift = 0;
        char        *_ptr  = nullptr;
    public:
        Proxy()     = delete;
        ~Proxy()    = default;

        Proxy( const Proxy& that )              = default;
        Proxy& operator=( const Proxy& that )   = default;

        Proxy( int shift, char* ptr );

        operator    bool() const;

        Proxy&      operator=( bool value );

        bool        valid() const;
    };

public:
    virtual Proxy       operator[]( int index )       pure;
    virtual const Proxy operator[]( int index ) const pure;
    
    virtual bool        valid()                 const pure;
    virtual size_t      size()                  const pure;
};

template <typename Data_T, size_t Size>
class StaticArray: public Array<Data_T>
{
private:
    char        _data[sizeof(Data_T) * Size] = {};

    size_t      _size   = Size;

public:
    StaticArray();
    StaticArray( const StaticArray& that )              = default;
    StaticArray& operator=( const StaticArray& that )   = default;

    ~StaticArray();

    explicit StaticArray( const Data_T& filler );

    Data_T&         operator[]( int index )       override;
    const Data_T&   operator[]( int index ) const override;
    
    bool            valid()                 const override;
    size_t          size()                  const override;

    Data_T*         begin() const;
    Data_T*         end()   const;
};

template <size_t Size>
class StaticArray<bool, Size>: public Array<bool>
{
private:
    char        _data[Size / 8 + 1] = {};

protected:
    size_t      _size   = Size;

public:
    StaticArray(): _size(Size) {};

    StaticArray( const StaticArray& that )              = default;
    StaticArray& operator=( const StaticArray& that )   = default;
    ~StaticArray()                                      = default;

    explicit StaticArray( bool filler );

    Proxy       operator[]( int index )       override;
    const Proxy operator[]( int index ) const override;
    
    bool        valid()                 const override;
    size_t      size()                  const override;
};

template <typename Data_T>
class DynamicArray: public Array<Data_T>
{
protected:
    char        *_data      = nullptr;

    size_t      _size       = 0;
    size_t      _capacity   = 0;

public:
    DynamicArray()  = default;

    DynamicArray( const DynamicArray& that );
    DynamicArray( DynamicArray&& that );
    DynamicArray( size_t size, const Data_T& filler );

    DynamicArray&   operator=( const DynamicArray& that );
    DynamicArray&   operator=( DynamicArray&& that );

    ~DynamicArray();

    size_t          capacity()              const;
    size_t          size()                  const override;

    Data_T&         operator[]( int index )       override;
    const Data_T&   operator[]( int index ) const override;
    
    bool            valid()                 const override;

    bool            resize( size_t new_capacity, bool force = false );

    Data_T          pop();
    size_t          push( const Data_T& value );

    Data_T*         begin() const;
    Data_T*         end()   const;

    bool            clear();
};

template<>
class DynamicArray<bool>: public Array<bool>
{
private:
    char        *_data      = nullptr;

    size_t      _size       = 0;
    size_t      _capacity   = 0;

public:
    DynamicArray()  = default;
    DynamicArray( const DynamicArray& that );
    DynamicArray( DynamicArray&& that );
    DynamicArray( size_t size, bool filler );

    DynamicArray&   operator=( const DynamicArray& that );
    DynamicArray&   operator=( DynamicArray&& that );

    ~DynamicArray();

    size_t      capacity()  const;
    size_t      size()      const override;

    Proxy       operator[]( int index )       override;
    const Proxy operator[]( int index ) const override;
    
    bool        valid() const override;
    bool        resize( size_t new_capacity, bool force = false);

    bool        pop();
    size_t      push( bool value );

    bool        clear();
};

#include "Array.hpp"

#endif //TRACK_CPP_2_SEM_ARRAY_H
