#ifdef  TRACK_CPP_2_SEM_ARRAY_H

#ifndef TRACK_CPP_2_SEM_ARRAY_HPP
#define TRACK_CPP_2_SEM_ARRAY_HPP

#include <new>
#include <cstring>
#include <cstdio>
#include "Array.h"
#include <utility>

//****************************************************************************
//      Array<bool>::Proxy

Array<bool>::Proxy::Proxy( int shift, char* ptr ):
        _shift( shift ),
        _ptr( ptr )
chainable
({
    if( ptr == nullptr )
        chained_throw( "Null pointer proxy construction", ChainedException::INVAVID_OBJ );
    if( !( shift >= 0 && shift < sizeof(char) * 8 ) )
        chained_throw( "Invalid byte shift proxy construction", ChainedException::INVAVID_OBJ );
})

Array<bool>::Proxy::operator bool() const chainable
({
    if( this->valid() )
    {
        return (bool)( (*_ptr) & (1 << _shift) );
    }
    return false;
})

Array<bool>::Proxy& Array<bool>::Proxy::operator=( bool value ) chainable
({
    if( valid() )
    {
       if( value )
       {
           ( *_ptr ) |= ( 1 << _shift );
       }
       else
       {
           ( *_ptr ) &= ~( 1 << _shift );
       }
       return *this;
    }
    return *this;
})

bool Array<bool>::Proxy::valid() const chainable
({
    if( _ptr == nullptr )
        chained_throw( "Null pointer proxy", ChainedException::INVAVID_OBJ );
    if( !( _shift >= 0 && _shift < sizeof(char) * 8 ) )
        chained_throw( "Invalid byte shift proxy", ChainedException::INVAVID_OBJ );
    return true;
})

//****************************************************************************
//      StaticArray<Data_T, Size>

template <typename Data_T, size_t Size>
StaticArray<Data_T, Size>::StaticArray() chainable
({
    if( valid() )
    {
        for( size_t i = 0; i < _size; i++ )
        {
            new ( _data + sizeof(Data_T) * i ) Data_T();
        }
    }
})

template <typename Data_T, size_t Size>
StaticArray<Data_T, Size>::~StaticArray() chainable
({
    for( size_t i = 0; i < _size; ++i )
    {
       ~(*(Data_T*)(_data + sizeof(Data_T) * i));
    }
})

template <typename Data_T, size_t Size>
StaticArray<Data_T, Size>::StaticArray( const Data_T &filler ) chainable
({
    for( size_t i = 0; i < _size; ++i )
    {
       new(_data + sizeof(Data_T) * i) Data_T( filler );
    }
})


template <typename Data_T, size_t Size>
const Data_T& StaticArray<Data_T, Size>::operator[]( int index ) const chainable
({
    if( valid() )
    {
        if( index < _size && index >= 0 )
        {
           return (*(Data_T*)(_data + sizeof(Data_T) * index ));
        }
        else chained_throw( "Index out of range", ChainedException::BAD_ARG );
    }
})

template <typename Data_T, size_t Size>
Data_T& StaticArray<Data_T, Size>::operator[]( int index ) chainable
({
    if( valid() )
    {
        if( index < _size && index >= 0 )
        {
            return (*(Data_T*)(_data + sizeof(Data_T) * index ));
        }
        else chained_throw( "Index out of range", ChainedException::BAD_ARG );
    }
})

template <typename Data_T, size_t Size>
bool StaticArray<Data_T, Size>::valid() const chainable
({
    if( _data != nullptr )
    { return true; }
    else chained_throw( "Memory error", ChainedException::INVAVID_OBJ );
})

template <typename Data_T, size_t Size>
size_t StaticArray<Data_T, Size>::size() const chainable
({
    valid();
    return _size;
})

template <typename Data_T, size_t Size>
Data_T* StaticArray<Data_T, Size>::begin() const chainable
({
    valid();
    return (Data_T*)_data;
})

template <typename Data_T, size_t Size>
Data_T* StaticArray<Data_T, Size>::end() const chainable
({
    if( valid() )
    { return (Data_T*)(_data + sizeof(Data_T) * _size ); }
    else chained_throw( "Array is invalid", ChainedException::INVAVID_OBJ );
})

//****************************************************************************
//      StaticArray<bool, Size>

template <size_t Size>
StaticArray<bool, Size>::StaticArray( bool filler ) chainable
({
    if( filler == true )
    {
       for( size_t i = 0; i < _size/8 + 1; i++ )
       {
           _data[i] |= 0xFF;
       }
    }
})

template <size_t Size>
const Array<bool>::Proxy StaticArray<bool, Size>::operator[]( int index ) const chainable
({
    valid();

    if( !( index >= 0 && index < _size ) )
        chained_throw( "Index out of range", ChainedException::BAD_ARG );
    
   Proxy   tmp( index % 8, (char*)(_data + index / 8));

   return  tmp;
})

template <size_t Size>
Array<bool>::Proxy StaticArray<bool, Size>::operator[]( int index ) chainable
({
    valid();
    
    if( !( index >= 0 && index < _size ) )
        chained_throw( "Index out of range", ChainedException::BAD_ARG );
    
    Proxy   tmp( index % 8, (char*)(_data + index / 8));
    
    return  tmp;
})

template <size_t Size>
bool StaticArray<bool, Size>::valid() const chainable
({
    if( _data == nullptr ) chained_throw( "Memory error", ChainedException::INVAVID_OBJ );
    return true;
})

template <size_t Size>
size_t StaticArray<bool, Size>::size() const chainable
({
    valid();
    return _size;
})

//****************************************************************************
//      DynamicArray<Data_T>


template <typename Data_T>
DynamicArray<Data_T>::DynamicArray( const DynamicArray &that ):
    _size( that._size ),
    _capacity( that._capacity )  chainable
({
    that.valid();
    
    _data   = new (std::nothrow) char[sizeof(Data_T) * _capacity]{};

    if( _data == nullptr ) chained_throw( "Memory allocation error", ChainedException::MEM_ERR );

    for( size_t i = 0; i < _size; ++i )
    {
       new( _data + sizeof( Data_T ) * i ) Data_T( that[i] );
    }
})

template <typename Data_T>
DynamicArray<Data_T>::DynamicArray( DynamicArray&& that ) chainable
({
    that.valid();
    
    std::swap(_size,     that._size);
    std::swap(_capacity, that._capacity);
    std::swap(_data,     that._data);
})

template <typename Data_T>
DynamicArray<Data_T>::DynamicArray( size_t size, const Data_T& filler ):
    _size( size ),
    _capacity( size ) chainable
({
    _data   = new (std::nothrow) char[sizeof(Data_T) * _size]{};

    if( _data == nullptr ) chained_throw( "Memory allocation error", ChainedException::MEM_ERR );
    
    for( size_t i = 0; i < _size; ++i )
    {
       new ( _data + sizeof(Data_T) * i ) Data_T( filler );
    }
})

template <typename Data_T>
DynamicArray<Data_T>& DynamicArray<Data_T>::operator=( const DynamicArray& that ) chainable
({
    if( this != &that )
    {
        that.valid();
        char* new_data = new( std::nothrow ) char[sizeof( Data_T ) * that._capacity]{};

        if( new_data == nullptr ) chained_throw( "Memory allocation error", ChainedException::MEM_ERR );

        for( size_t i = 0; i < that._size; i++ )
        {
           new( new_data + sizeof( Data_T ) * i ) Data_T( that[i] );
        }

        for( size_t i = 0; i < _size; ++i )
        {
           ((Data_T*)(_data + sizeof(Data_T) * i))->~Data_T();
        }
        delete[] _data;

        _data        = new_data;
        _size        = that._size;
        _capacity    = that._capacity;

        return *this;
    }
    return *this;
})

template <typename Data_T>
DynamicArray<Data_T>& DynamicArray<Data_T>::operator=( DynamicArray&& that ) chainable
({
    that.valid();
    
    new (this) DynamicArray( std::move(that) );
    return *this;
})

template <typename Data_T>
DynamicArray<Data_T>::~DynamicArray() chainable
({
    if( _data != nullptr )
    {
        for( size_t i = 0; i < _size; ++i )
        {
           ((Data_T*)(_data + sizeof(Data_T) * i))->~Data_T();
        }
        delete[] _data;
    }
    
    _data       = nullptr;
    _size       = 0;
    _capacity   = 0;
})

template <typename Data_T>
size_t DynamicArray<Data_T>::capacity() const chainable
({
    valid();
    return _capacity;
})

template <typename Data_T>
size_t DynamicArray<Data_T>::size() const chainable
({
    valid();
    return _size;
})

template <typename Data_T>
const Data_T& DynamicArray<Data_T>::operator[]( int index ) const chainable
({
    valid();

    if( !( index < _size && index >= 0 ) )
        chained_throw( "Index out of range", ChainedException::BAD_ARG );
       
    return (*(Data_T*)(_data + sizeof(Data_T) * index ));
})

template <typename Data_T>
Data_T& DynamicArray<Data_T>::operator[]( int index ) chainable
({
    valid();
    
    if( !( index < _size && index >= 0 ) )
        chained_throw( "Index out of range", ChainedException::BAD_ARG );
    
    return (*(Data_T*)(_data + sizeof(Data_T) * index ));
})

template <typename Data_T>
bool DynamicArray<Data_T>::valid() const chainable
({
    if( _size > _capacity )
        chained_throw( "Data array overflow", ChainedException::INVAVID_OBJ );

    if( _capacity != 0 && _data == nullptr)
       chained_throw( "Data array is null", ChainedException::INVAVID_OBJ );

    return true;
})

template <typename Data_T>
bool DynamicArray<Data_T>::resize( size_t new_capacity, bool force ) chainable
({
    valid();
    
    if( !force && new_capacity < _size )
        chained_throw( "Shrinking array involves data loss", ChainedException::BAD_ARG );

    char*    new_data = new (std::nothrow) char[sizeof(Data_T) * new_capacity]{};

    if( new_data == nullptr ) chained_throw( "Memory allocation error", ChainedException::MEM_ERR );

    std::memcpy( new_data, _data, sizeof(Data_T) * _size );

    delete[] _data;

    _data        = new_data;
    _capacity    = new_capacity;

    return true;
})

template <typename Data_T>
Data_T DynamicArray<Data_T>::pop() chainable
({
    valid();
    if( _size == 0 ) chained_throw( "Pop from empty array", ChainedException::BAD_ARG );

    Data_T tmp( operator[](_size - 1) );
    _size    -=  1;

    ((Data_T*)(_data + sizeof(Data_T) * _size))->~Data_T();

    return tmp;
})

template <typename Data_T>
size_t DynamicArray<Data_T>::push( const Data_T &value ) chainable
({
    valid();
    if( _size == _capacity ) chained_throw( "Array overflow", ChainedException::BAD_ARG );

    new ( _data + sizeof(Data_T) * _size ) Data_T( value );
    _size++;

    return _size;
})

template <typename Data_T>
Data_T* DynamicArray<Data_T>::begin() const chainable
({
    valid();
    return (Data_T*)_data;
})

template <typename Data_T>
Data_T* DynamicArray<Data_T>::end() const chainable
({
    valid();
    return (Data_T*)(_data + sizeof(Data_T) * _size );
})

template <typename Data_T>
bool    DynamicArray<Data_T>::clear() chainable
({
    valid();
    for( size_t i = 0; i < _size; ++i )
    {
        ((Data_T*)( _data + sizeof(Data_T) * i ))->~Data_T();
    }
    _size = 0;
    return true;
})

//****************************************************************************
//      DynamicArray<bool>

DynamicArray<bool>::DynamicArray( const DynamicArray<bool>& that ):
    _size( that._size ),
    _capacity( that._capacity ) chainable
({
    that.valid();
    
    _data   = new (std::nothrow) char[_capacity / 8  + 1]{};
    if( _data == nullptr ) chained_throw( "Memory allocation error", ChainedException::MEM_ERR );

    std::memcpy( _data, that._data, _size / 8  + 1 );
    
})

DynamicArray<bool>::DynamicArray( DynamicArray<bool> &&that ) chainable
({
    that.valid();
    
    std::swap( _size,     that._size );
    std::swap( _capacity, that._capacity );
    std::swap( _data,     that._data );
})

DynamicArray<bool>::DynamicArray( size_t size, bool filler ):
    _size( size ),
    _capacity( size ) chainable
({
    _data   = new (std::nothrow) char[_size / 8  + 1]{};
    if( _data == nullptr ) chained_throw( "Memory allocation error", ChainedException::MEM_ERR );
    
    if( filler )
    {
       for( size_t i = 0; i < _size; ++i )
           _data[i] = (char)0xFF;
    }
})

DynamicArray<bool>& DynamicArray<bool>::operator=( const DynamicArray& that ) chainable
({
    that.valid();
    
    char* new_data   = new (std::nothrow) char[that._capacity / 8  + 1]{};
    if( new_data == nullptr ) chained_throw( "Memory allocation error", ChainedException::MEM_ERR );

    std::memcpy( new_data, that._data, that._capacity / 8 + 1 );

    delete[]     _data;

    _data       = new_data;
    _size       = that._size;
    _capacity   = that._capacity;

    return *this;
})

DynamicArray<bool>& DynamicArray<bool>::operator=( DynamicArray&& that ) chainable
({
    that.valid();
    new (this) DynamicArray( std::move( that ) );
    return *this;
})

DynamicArray<bool>::~DynamicArray() chainable
({
    if( _data != nullptr )
        delete[] _data;
    _data = nullptr;
    _capacity = 0;
    _size = 0;
})

size_t DynamicArray<bool>::capacity() const chainable
({
    valid();
    return _capacity;
})

size_t DynamicArray<bool>::size() const chainable
({
    valid();
    return _size;
})

const Array<bool>::Proxy DynamicArray<bool>::operator[]( int index ) const chainable
({
    if( !( index >= 0 && index < _size ) )
        chained_throw( "Index out of range", ChainedException::BAD_ARG );
    
    Proxy   tmp( index % 8, (char*)(_data + index / 8));

    return  tmp;
})

Array<bool>::Proxy DynamicArray<bool>::operator[]( int index ) chainable
({
    if( !( index >= 0 && index < _size ) )
        chained_throw( "Index out of range", ChainedException::BAD_ARG );
    
    Proxy   tmp( index % 8, (char*)(_data + index / 8));
    
    return  tmp;
})

bool DynamicArray<bool>::valid() const chainable
({
    if( _size > _capacity )
        chained_throw( "Data array overflow", ChainedException::INVAVID_OBJ );
    if( _capacity != 0 && _data == nullptr)
        chained_throw( "Data array is null", ChainedException::INVAVID_OBJ );
    
    return true;
})

bool DynamicArray<bool>::resize( size_t new_capacity, bool force ) chainable
({
    valid();
    
    if( !force && new_capacity < _size )
        chained_throw( "Shrinking array involves data loss", ChainedException::BAD_ARG );

    char*    new_data = new (std::nothrow) char[new_capacity / 8 + 1]{};

    if( new_data == nullptr ) chained_throw( "Memory allocation error", ChainedException::MEM_ERR );

    std::memcpy( new_data, _data, _size / 8 + 1 );

    delete[] _data;

    _data        = new_data;
    _capacity    = new_capacity;

    return true;
})

bool DynamicArray<bool>::pop() chainable
({
    valid();
    
    if( _size == 0 ) chained_throw( "Pop from empty array", ChainedException::BAD_ARG );

    bool result = operator[]( (int)_size - 1 );

    _size--;

    return result;
})

size_t DynamicArray<bool>::push( bool value ) chainable
({
    valid();
    
    if( _size == _capacity ) chained_throw( "Array overflow", ChainedException::BAD_ARG );

    _size++;

    operator[]( (int)_size - 1 ) = value;

    return _size;

})

bool    DynamicArray<bool>::clear() chainable
({
    valid();
    _size    = 0;
    return true;
})

#endif //TRACK_CPP_2_SEM_ARRAY_HPP

#endif //TRACK_CPP_2_SEM_ARRAY_H
